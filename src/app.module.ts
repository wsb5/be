import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AuthMiddleware } from './common/middlewares';
import { JwtModule } from '@nestjs/jwt';
import { environment } from './environments/environment';
import { ToolsModule } from './tools/tools.module';
import { CategoriesModule } from './categories/categories.module';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      autoLoadEntities: true,
      synchronize: true,
    }),
    AuthModule,
    UsersModule,
    JwtModule.register({
      secret: process.env.SECRET_KEY,
    }),
    ToolsModule,
    CategoriesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  constructor(private readonly _connection: Connection) {}

  public configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(AuthMiddleware)
      .exclude(`${environment.apiPrefix}/auth/login`)
      .forRoutes('*');
  }
}
