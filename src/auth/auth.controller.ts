import { Body, Controller, Get, HttpCode, Post, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { IdentityDto, RefreshTokenDto } from './dto';
import { BaseController } from '../common/controllers';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

@Controller('auth')
export class AuthController extends BaseController {

  constructor(
    protected readonly jwtService: JwtService,
    private readonly _authService: AuthService
  ) {
    super(jwtService);
  }

  @Get('status')
  public async getStatus(): Promise<void> {}

  @Post('login')
  @HttpCode(200)
  public async login(@Body('username') username: string, @Body('password') password: string): Promise<IdentityDto> {
    return await this._authService.validateCredentials({ username, password });
  }

  @Post('refresh-token')
  @HttpCode(200)
  public async refreshToken(@Req() req: Request): Promise<RefreshTokenDto> {
    return await this._authService.getRefreshToken(super.getUserIdFromToken(req.headers.authorization));
  }
}
