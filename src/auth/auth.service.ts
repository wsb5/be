import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../users/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { IdentityDto, RefreshTokenDto } from './dto';
import { JwtService } from '@nestjs/jwt';
import { environment } from '../environments/environment';
import { InternalCodesEnum } from './enums';

@Injectable()
export class AuthService {

  constructor(
    @InjectRepository(UserEntity) private readonly _userRepository: Repository<UserEntity>,
    private readonly _jwtService: JwtService
  ) {}

  public async validateCredentials(credentials: { username: string, password: string }): Promise<IdentityDto> {
    const fUser= await this._userRepository.findOne({
      username: credentials.username,
      isDeleted: false
    });

    if (fUser && await bcrypt.compare(process.env.SECRET_KEY + credentials.password, fUser.password)) {
      const token = await this.getToken(environment.jwt.token.expiresIn, { id: fUser.id });
      const refreshToken = await this.getToken(environment.jwt.refreshToken.expiresIn, { id: fUser.id });

      return new IdentityDto(fUser.id, fUser.firstName, fUser.lastName, fUser.username, fUser.email, token, refreshToken);
    } else {
      throw new HttpException(
        {
          message: 'Invalid credentials.',
          internalCode: InternalCodesEnum.InvalidCredentials
        },
        HttpStatus.BAD_REQUEST
      );
    }
  }

  public async getRefreshToken(id: number): Promise<RefreshTokenDto> {
    return new RefreshTokenDto(await this.getToken(environment.jwt.token.expiresIn, { id }));
  }

  private getToken(expiresIn: string, payload: object): Promise<string> {
    return this._jwtService.signAsync(payload, { expiresIn, secret: process.env.SECRET_KEY });
  }
}
