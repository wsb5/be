export class IdentityDto {
  constructor(
    public readonly id: number,
    public readonly firstName: string,
    public readonly lastName: string,
    public readonly username: string,
    public readonly email: string,
    public readonly token: string,
    public readonly refreshToken: string,
  ) {}
}
