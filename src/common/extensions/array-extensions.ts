export {};

Array.prototype.sortByKey = function(key: string): any[] {
  return this.sort((a: any, b: any) => a[key] < b[key] ? -1 : 1);
};
