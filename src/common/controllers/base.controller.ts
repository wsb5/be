import { JwtService } from '@nestjs/jwt';

export abstract class BaseController {
  protected constructor(
    protected readonly jwtService: JwtService
  ) {}

  protected getUserIdFromToken(authorization: string): number {
    return this.jwtService.decode(authorization.split(' ')[1])['id'];
  }
}
