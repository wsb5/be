import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { JwtService } from '@nestjs/jwt';
import { InternalCodesEnum } from '../enums';

@Injectable()
export class AuthMiddleware implements NestMiddleware {

  constructor(
    private readonly _jwtService: JwtService
  ) {}

  public async use(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      await this._jwtService.verifyAsync(req.headers.authorization.split(' ')[1], { secret: process.env.SECRET_KEY });
      next();
    } catch (e) {
      throw new HttpException(
        {
          message: 'Token expired.',
          internalCode: InternalCodesEnum.TokenExpired
        },
        HttpStatus.UNAUTHORIZED
      );
    }
  }
}
