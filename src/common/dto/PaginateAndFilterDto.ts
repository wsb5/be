import { ApiProperty } from '@nestjs/swagger';

export class PaginateAndFilterDto {
  constructor(page: number, results: number, search: string) {
    this.page = page;
    this.results = results;
    this.search = search;
  }
  @ApiProperty()
  page: number;
  @ApiProperty()
  results: number;
  @ApiProperty()
  search: string;
}
