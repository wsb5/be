export class TableResultsDto<T> {

  constructor(results: T[], info: { page: number; results: number; totalResults: number }) {
    this.results = results;
    this.info = info;
  }

  results: T[];
  info: {
    page: number;
    results: number;
    totalResults: number;
  };
}
