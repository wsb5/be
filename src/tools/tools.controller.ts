import { Controller, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { ToolsService } from './tools.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import * as moment from 'moment';
import { UploadedFilenameDto } from './dto';
import { Request } from 'express';
import { environment } from '../environments/environment';
import { FileDirectoryTypes } from './enums';

@Controller('tools')
export class ToolsController {

  constructor(
    private readonly _toolsService: ToolsService
  ) {}

  @Post('upload-file')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: (req: Request, file: Express.Multer.File, callback: (error: Error | null, destination: string) => void) => {
          callback(null, `${ environment.publicDirectoryPath }/${ FileDirectoryTypes[req.query['type'].toString()].toLowerCase() }`);
        },
        filename: (req, file, cb) => cb(null, `${ moment().valueOf() }-${ file.originalname }`)
      })
    })
  )
  public uploadImage(@UploadedFile() file: Express.Multer.File): UploadedFilenameDto {
    return this._toolsService.getUploadedFilename(file);
  }
}
