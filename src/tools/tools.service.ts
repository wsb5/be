import { HttpService, Injectable } from '@nestjs/common';
import { UploadedFilenameDto } from './dto';
import { map } from 'rxjs/operators';
import * as fs from 'fs';
import { environment } from '../environments/environment';
import { FileDirectoryTypes } from './enums';
import { Observable } from 'rxjs';

@Injectable()
export class ToolsService {
  constructor(private readonly _http: HttpService) {}

  public getUploadedFilename(file: Express.Multer.File): UploadedFilenameDto {
    return new UploadedFilenameDto(file.filename);
  }

  public downloadAndSaveImage(url: string, name: string): Observable<any> {
    return this._http.get(url, { responseType: 'stream' }).pipe(
      map((response) => {
        response.data.pipe(
          fs.createWriteStream(
            `${environment.publicDirectoryPath}/${FileDirectoryTypes.IMAGES}/${name}`,
          ),
        );
      }),
    );
  }
}
