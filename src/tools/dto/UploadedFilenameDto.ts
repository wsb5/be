export class UploadedFilenameDto {
  constructor(
    public readonly filename: string
  ) {}
}
