export const environment = {
  apiPrefix: 'api/v1',
  publicDirectoryPath: './public',
  jwt: {
    token: {
      expiresIn: '1h',
    },
    refreshToken: {
      expiresIn: '12h',
    },
  },
};
