import { Body, Controller, Delete, Get, HttpCode, Post, Put, Req } from '@nestjs/common';
import { Request } from 'express';
import { CategoryDto } from './dto';
import { CategoriesService } from './categories.service';

@Controller('categories')
export class CategoriesController {

  constructor(
    private readonly _categoriesService: CategoriesService
  ) {}

  @Get()
  public async getAll(@Req() request: Request): Promise<CategoryDto[]> {
    return await this._categoriesService.getAll(request.query['search'] ? request.query['search'].toString() : '');
  }

  @Get('tree-list')
  public async getAllAsTree(@Req() request: Request): Promise<CategoryDto[]> {
    return await this._categoriesService.getAllAsTree(request.query['search'] ? request.query['search'].toString() : '');
  }

  @Get(':id')
  public async getById(@Req() request: Request): Promise<CategoryDto> {
    return await this._categoriesService.getById(request.params.id);
  }

  @Post()
  public async create(
    @Body('parentId') parentId: number,
    @Body('key') key: string,
    @Body('name') name: string,
    @Body('imageKey') imageKey: string,
    @Body('description') description: string,
  ): Promise<void> {
    await this._categoriesService.create(parentId, key, name, imageKey, description);
  }

  @Put(':id')
  public async update(
    @Req() request: Request,
    @Body('parentId') parentId: number,
    @Body('key') key: string,
    @Body('name') name: string,
    @Body('imageKey') imageKey: string,
    @Body('description') description: string,
  ): Promise<void> {
    await this._categoriesService.update(request.params.id, parentId, key, name, imageKey, description);
  }

  @Delete(':id')
  @HttpCode(204)
  public async deleteById(@Req() request: Request): Promise<void> {
    await this._categoriesService.delete(request.params.id);
  }
}
