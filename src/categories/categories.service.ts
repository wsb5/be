import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Category } from './category.entity';
import { CategoryDto } from './dto';
import { InternalCodesEnum } from './enums';

@Injectable()
export class CategoriesService {

  constructor(
    @InjectRepository(Category) private readonly _categoryRepository: Repository<Category>
  ) {}

  public async getAll(search: string): Promise<CategoryDto[]> {
    try {
      const categories = await this._categoryRepository.createQueryBuilder('category')
      .where(`category.id like :search`, { search: `%${ search }%` })
      .orWhere(`category.parent_id like :search`, { search: `%${ search }%` })
      .orWhere(`category.name like :search`, { search: `%${ search }%` })
      .orWhere(`category.description like :search`, { search: `%${ search }%` })
      .orWhere(`category.created_at like :search`, { search: `%${ search }%` })
      .orderBy('category.id', 'DESC')
      .getMany();

      const categoriesCollection = categories.map(category => {
        return new CategoryDto(
          category.id,
          category.parentId,
          category.key,
          category.name,
          category.imageKey,
          category.description,
          category.createdAt,
          []
        );
      });

      categoriesCollection.forEach(category => category.children = categoriesCollection.filter(a => a.parentId === category.id).sortByKey('key'));
      return categoriesCollection.sortByKey('key');
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async getAllAsTree(search: string): Promise<CategoryDto[]> {
    const categories = await this.getAll(search);
    return categories.filter(a => a.parentId === null);
  }

  public async getById(id: string): Promise<CategoryDto> {
    const fCategory = await this._categoryRepository.findOne({ id: parseInt(id, 10) });

    if (!fCategory) {
      throw new HttpException(
        {
          message: `Category with id ${ id } doesn't exist.`,
          internalCode: InternalCodesEnum.CategoryNotFound
        },
        HttpStatus.NOT_FOUND
      );
    }

    const categories = await this.getAll('');

    try {
      return new CategoryDto(
        fCategory.id,
        fCategory.parentId,
        fCategory.key,
        fCategory.name,
        fCategory.imageKey,
        fCategory.description,
        fCategory.createdAt,
        categories.find(a => a.id === parseInt(id, 10)).children.sortByKey('key')
      );
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async create(parentId: number, key: string, name: string, imageKey: string, description: string): Promise<void> {
    const category = new Category();

    category.parentId = parentId;
    category.key = key;
    category.name = name;
    category.imageKey = imageKey;
    category.description = description;

    try {
      await this._categoryRepository.save(category);
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async update(id: string, parentId: number, key: string, name: string, imageKey: string, description: string): Promise<void> {
    const fCategory = await this._categoryRepository.findOne({ id: parseInt(id, 10) });

    if (!fCategory) {
      throw new HttpException(
        {
          message: `Category with id ${ id } doesn't exist.`,
          internalCode: InternalCodesEnum.CategoryNotFound
        },
        HttpStatus.NOT_FOUND
      );
    }

    fCategory.parentId = parentId;
    fCategory.key = key;
    fCategory.name = name;
    fCategory.imageKey = imageKey;
    fCategory.description = description;

    try {
      await this._categoryRepository.save(fCategory);
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async delete(id: string): Promise<void> {
    const fCategory = await this._categoryRepository.findOne({ id: parseInt(id , 10) });

    if (!fCategory) {
      throw new HttpException(
        {
          message: `Category with id ${ id } doesn't exist.`,
          internalCode: InternalCodesEnum.CategoryNotFound
        },
        HttpStatus.NOT_FOUND
      );
    }

    const category = await this.getById(id);
    const ids: number[] = [parseInt(id, 10)];
    const getIds = (children: CategoryDto[]) => {
      children.forEach(el => {
        ids.push(el.id);
        getIds(el.children);
      });
    };

    getIds(category.children);

    try {
      const promises: Promise<Category>[] = [];
      ids.forEach(async id => promises.push(this._categoryRepository.remove(await this._categoryRepository.findOne({ id }))));
      await Promise.all(promises);
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
