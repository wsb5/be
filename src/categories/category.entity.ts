import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('categories')
export class Category {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ name: 'parent_id', nullable: true })
  public parentId: number;

  @Column()
  public key: string;

  @Column()
  public name: string;

  @Column({ type: 'longtext', default: null })
  public description: string;

  @Column({ name: 'image_key', default: null })
  public imageKey: string;

  @CreateDateColumn({ name: 'created_at', type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
  public createdAt: Date;
}
