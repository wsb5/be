export class CategoryDto {
  constructor(
    public readonly id: number,
    public readonly parentId: number,
    public readonly key: string,
    public readonly name: string,
    public readonly imageKey: string,
    public readonly description: string,
    public readonly createdAt: Date,
    public children: CategoryDto[]
  ) {}
}
