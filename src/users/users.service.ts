import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { GeneratedPasswordDto, UserDto } from './dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { Brackets, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { InternalCodesEnum } from './enums';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(UserEntity) private readonly _userRepository: Repository<UserEntity>
  ) {}

  public async getAll(search: string): Promise<UserDto[]> {
    try {
      const users = await this._userRepository.createQueryBuilder('user')
      .where(`user.isDeleted != :deleted`, { deleted: true })
      .andWhere(new Brackets((qb => {
        qb.where(`user.id like :search`, { search: `%${ search }%` })
        .orWhere(`user.firstName like :search`, { search: `%${ search }%` })
        .orWhere(`user.lastName like :search`, { search: `%${ search }%` })
        .orWhere(`user.username like :search`, { search: `%${ search }%` })
        .orWhere(`user.email like :search`, { search: `%${ search }%` })
      })))
      .orderBy('user.id', 'DESC')
      .getMany();

      return users.map(({ id, firstName, lastName, username, email }) => {
        return new UserDto(id, firstName, lastName, username, email);
      });
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async getById(id: string): Promise<UserDto> {
    const fUser = await this._userRepository.findOne({ id: parseInt(id, 10), isDeleted: false });

    if (!fUser) {
      throw new HttpException(
        {
          message: `User with id ${ id } doesn't exist.`,
          internalCode: InternalCodesEnum.UserNotFound
        },
        HttpStatus.NOT_FOUND
      );
    }

    try {
      return new UserDto(fUser.id, fUser.firstName, fUser.lastName, fUser.username, fUser.email);
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async create(firstName: string, lastName: string, username: string, email: string): Promise<GeneratedPasswordDto> {
    if (await this._userRepository.findOne({ username, isDeleted: false })) {
      throw new HttpException(
        {
          message: 'User with this username already exists.',
          internalCode: InternalCodesEnum.UsernameInUse
        },
        HttpStatus.UNPROCESSABLE_ENTITY
      );
    }

    if (await this._userRepository.findOne({ email, isDeleted: false })) {
      throw new HttpException(
        {
          message: 'User with this email already exists.',
          internalCode: InternalCodesEnum.EmailInUse
        },
        HttpStatus.UNPROCESSABLE_ENTITY
      );
    }

    const user = new UserEntity();
    const generatedPassword = this.generatePassword();

    user.firstName = firstName;
    user.lastName = lastName;
    user.username = username;
    user.email = email;
    user.password = await bcrypt.hash(process.env.SECRET_KEY + generatedPassword, 10);

    try {
      await this._userRepository.save(user);
      return new GeneratedPasswordDto(generatedPassword);
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async update(id: string, firstName: string, lastName: string, username: string, email: string): Promise<void> {
    const fUser = await this._userRepository.findOne({ id: parseInt(id, 10), isDeleted: false });

    if (!fUser) {
      throw new HttpException(
        {
          message: `User with id ${ id } doesn't exist.`,
          internalCode: InternalCodesEnum.UserNotFound
        },
        HttpStatus.NOT_FOUND
      );
    }

    const fUserByUserName = await this._userRepository.findOne({ username, isDeleted: false });

    if (fUserByUserName && fUserByUserName.id !== fUser.id) {
      throw new HttpException(
        {
          message: 'User with this username already exists.',
          internalCode: InternalCodesEnum.UsernameInUse
        },
        HttpStatus.UNPROCESSABLE_ENTITY
      );
    }

    const fUserByEmail = await this._userRepository.findOne({ email, isDeleted: false });

    if (fUserByEmail && fUserByEmail.id !== fUser.id) {
      throw new HttpException(
        {
          message: 'User with this email already exists.',
          internalCode: InternalCodesEnum.EmailInUse
        },
        HttpStatus.UNPROCESSABLE_ENTITY
      );
    }

    fUser.firstName = firstName;
    fUser.lastName = lastName;
    fUser.username = username;
    fUser.email = email;

    try {
      await this._userRepository.save(fUser);
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async delete(id: string): Promise<void> {
    const fUser = await this._userRepository.findOne({ id: parseInt(id , 10), isDeleted: false });

    if (!fUser) {
      throw new HttpException(
        {
          message: `User with id ${ id } doesn't exist.`,
          internalCode: InternalCodesEnum.UserNotFound
        },
        HttpStatus.NOT_FOUND
      );
    }

    fUser.isDeleted = true;

    try {
      await this._userRepository.save(fUser);
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public async resetPassword(id: number): Promise<GeneratedPasswordDto> {
    const fUser = await this._userRepository.findOne({ id: id });

    if (!fUser) {
      throw new HttpException(
        {
          message: `User with id ${ id } doesn't exist.`,
          internalCode: InternalCodesEnum.UserNotFound
        },
        HttpStatus.NOT_FOUND
      );
    }

    const generatedPassword = this.generatePassword();
    fUser.password = await bcrypt.hash(process.env.SECRET_KEY + generatedPassword, 10);

    try {
      await this._userRepository.save(fUser);
      return new GeneratedPasswordDto(generatedPassword);
    } catch (e) {
      throw new HttpException(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private generatePassword(passwordLength = 30): string {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()';
    return Array.apply(null, Array(passwordLength)).map(() => chars[Math.floor(Math.random() * (chars.length - 1))]).join('');
  }
}

