export enum InternalCodesEnum {
  UserNotFound = 'UserNotFound',
  UsernameInUse = 'UsernameInUse',
  EmailInUse = 'EmailInUse',
}
