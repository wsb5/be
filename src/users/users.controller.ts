import { Body, Controller, Delete, Get, HttpCode, Post, Put, Req } from '@nestjs/common';
import { UsersService } from './users.service';
import { GeneratedPasswordDto, UserDto } from './dto';
import { Request } from 'express';
import { BaseController } from '../common/controllers';
import { JwtService } from '@nestjs/jwt';

@Controller('users')
export class UsersController extends BaseController {

  constructor(
    protected readonly jwtService: JwtService,
    private readonly _usersService: UsersService,
  ) {
    super(jwtService);
  }

  @Get()
  public async getAll(@Req() request: Request): Promise<UserDto[]> {
    return await this._usersService.getAll(request.query['search'] ? request.query['search'].toString() : '');
  }

  @Get(':id')
  public async getById(@Req() request: Request): Promise<UserDto> {
    return await this._usersService.getById(request.params.id);
  }

  @Post()
  public async create(
    @Body('firstName') firstName: string,
    @Body('lastName') lastName: string,
    @Body('username') username: string,
    @Body('email') email: string,
  ): Promise<GeneratedPasswordDto> {
    return await this._usersService.create(firstName, lastName, username, email);
  }

  @Put(':id')
  public async update(
    @Req() request: Request,
    @Body('firstName') firstName: string,
    @Body('lastName') lastName: string,
    @Body('username') username: string,
    @Body('email') email: string,
  ): Promise<void> {
    await this._usersService.update(request.params.id, firstName, lastName, username, email);
  }

  @Delete(':id')
  @HttpCode(204)
  public async deleteById(@Req() request: Request): Promise<void> {
    await this._usersService.delete(request.params.id);
  }

  @Post('reset-password')
  @HttpCode(200)
  public async resetPassword(@Req() request: Request): Promise<GeneratedPasswordDto> {
    return await this._usersService.resetPassword(super.getUserIdFromToken(request.headers.authorization));
  }
}
