import {MigrationInterface, QueryRunner} from "typeorm";

export class InitRoot1625170920697 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.manager.createQueryBuilder().insert().into('users').values({
            username: 'root',
            email: 'root@root.com',
            password: '$2b$10$Y/yx.Q/ZFGZJUtSk42hLreC/xTkRRJZGor9Jth0Nrj/0p9sFnMkYW',
            firstName: 'root',
            lastName: 'root'
        }).execute();
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
