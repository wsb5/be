1) Open project directory in terminal
2) Use command "yarn" to install all dependencies
3) Open file ormconfig.example and set all values according to your database connection
4) Rename ormconfig.example file to ormconfig.json
5) Run command "yarn start"
6) Run command "yarn typeorm migration:run"

API is running.
